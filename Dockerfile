FROM debian:buster-slim

ADD ninvaders-0.1.1 /root/ninvaders-0.1.1
RUN apt -y update && apt -y install build-essential libncurses5-dev
RUN ls -la /root/ && cd /root/ninvaders-0.1.1 && make 

CMD "/root/ninvaders-0.1.1/nInvaders"
