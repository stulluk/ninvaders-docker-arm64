# ninvaders-docker-arm64

This repository builds a simple docker arm64 container which runs ninvaders game.

Additionally, I used docker buildx to create a container which can run for amd64 also.

# Prequisities

you only need to install docker.

# Building

You don't need to build anything. Just run it.

# Running the game

docker run -it stulluk/ninvaders-docker-arm64

This will pull the containers from docker hub.

# Playing

Press space to start the game, and shoot aliens.

Move left / right via arrow buttons on your keyboard.

Press "q" to exit the game and the container.

# Test

Tested on Pine64 and amd64 PC.
